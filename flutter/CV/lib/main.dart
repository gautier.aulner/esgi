import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const GautierCVApp());
}

class GautierCVApp extends StatelessWidget {
  const GautierCVApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CV - Gautier Aulner',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const GautierCVScreen(),
    );
  }
}

class GautierCVScreen extends StatelessWidget {
  const GautierCVScreen({Key? key}) : super(key: key);

  Future<void> _gotoLink(String url) async {
    final urlUri = Uri.parse(url);

    await launchUrl(urlUri);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mon CV'),
        backgroundColor: Colors.brown[400],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const Center(
              child: CircleAvatar(
                radius: 50.0,
                backgroundImage: AssetImage('assets/profile_picture.jpg'),
              ),
            ),
            const SizedBox(height: 20.0),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.brown,
                    width: 1.0,
                  ),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: const Center(
                  child: Column(
                    children: [
                      Text(
                        'AULNER Gautier',
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        'Software Engineer',
                        style: TextStyle(
                          fontSize: 18.0,
                          fontStyle: FontStyle.italic,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            const Text(
              'A propos de moi',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const Text(
              'Je suis un développeur passioné par le Web. Je travaille actuellment chez FundsDLT en tant que développeur Front-end en utilisant SvelteKit et GraphQL.',
              style: TextStyle(fontSize: 16.0),
            ),
            const SizedBox(height: 30.0),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Card(
                  color: Colors.orange,
                  elevation: 4.0,
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: SizedBox(
                      width: 300,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Compétences techniques:',
                            style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                          Text(
                            'SvelteKit',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                          Slider(
                            value: 80,
                            min: 0.0,
                            max: 100.0,
                            onChanged: null, // Disables the slider
                          ),
                          Text(
                            'HTML, CSS, JavaScript, TypeScript',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                          Slider(
                            value: 80,
                            min: 0.0,
                            max: 100.0,
                            onChanged: null, // Disables the slider
                          ),
                          Text(
                            'PHP',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                          Slider(
                            value: 50,
                            min: 0.0,
                            max: 100.0,
                            onChanged: null, // Disables the slider
                          ),
                          Text(
                            'SQL',
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w500,
                              color: Colors.white,
                            ),
                          ),
                          Slider(
                            value: 60,
                            min: 0.0,
                            max: 100.0,
                            onChanged: null, // Disables the slider
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Card(
                  color: Colors.black87,
                  elevation: 4.0,
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Experiences professionnelles',
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2023 -> Aujourd'hui\n Alternance • FundsDLT, Esch-Belval • Développeur d'application Web",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          'Développement sur une application de gestion de fonds',
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2022 : 3 mois\n Stage • SDIS 57 pole DFAC • Développeur de logiciel Web",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Création d'un logiciel de gestion de livret stagiaire avec Symfony",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2019 -> 2022\n SDIS 57 • Sapeurs-Pompiers Volontaires",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2021 : 2 mois\n Stage • Happiso, Metz • Développeur de logiciel Web",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Développement d'un module Web utilisant les WebSockets grâce à PHP et le framework CakePHP",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2019 -> 2020\n McDonald's, Semecourt • Equipier",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "Développement d'un module Web utilisant les WebSockets grâce à PHP et le framework CakePHP",
                          style: TextStyle(
                            fontSize: 16.0,
                            color: Colors.white,
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          "• 2019\n Metz • Participation à un Hackathon",
                          style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 20.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.brown[500],
                    ),
                    onPressed: () {},
                    child: const Row(
                      children: [
                        Icon(Icons.phone),
                        Text(
                          '06 12 34 56 78',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.brown[500],
                    ),
                    onPressed: () {},
                    child: const Row(
                      children: [
                        Icon(Icons.download),
                        Text('Téléchargez mon CV')
                      ],
                    )),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.brown[500],
                    ),
                    onPressed: () => _gotoLink(
                        'https://www.linkedin.com/in/gautier-aulner-a93533182/'),
                    child: const Row(
                      children: [
                        Icon(
                          Icons.people,
                          color: Colors.blueAccent,
                        ),
                        Text(
                          'LinkedIn',
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    )),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
