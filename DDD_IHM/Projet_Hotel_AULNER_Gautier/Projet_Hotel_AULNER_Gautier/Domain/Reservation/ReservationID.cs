﻿using Projet_Hotel_AULNER_Gautier.Domain.Customer;
using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.Reservation
{
    public class ReservationID
    {
        [Required]
        public Guid Value { get; }

        public ReservationID(Guid value)
        {
            Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is ReservationID otherID)
            {
                return Value == otherID.Value;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
