﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.Reservation
{
    public interface ReservationRepository
    {
        //public bool RemoveReservation(Guid id);
        public List<Reservation> GetReservations();
        public Reservation? GetReservationById(Guid id);
        public void SaveReservations(List<Reservation> reservations);
        public bool UpdateReservation(Guid id, Reservation reservation);

    }
}
