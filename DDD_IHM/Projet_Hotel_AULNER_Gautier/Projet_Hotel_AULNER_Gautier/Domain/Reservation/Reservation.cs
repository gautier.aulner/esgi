﻿using Projet_Hotel_AULNER_Gautier.Domain.Attribute;
using Projet_Hotel_AULNER_Gautier.Domain.Customer;
using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.Reservation
{
    public class Reservation
    {
        //[Required]
        public ReservationID ReservationID { get; set; }

        [Required]
        public CustomerID CustomerID { get; set; }

        [Required]
        [CustomDateTime]
        public DateTime CheckInDate { get; set; }

        [Required]
        [Range(0, int.MaxValue)]
        public int NumberOfNights { get; set; }

        public enum ReservationStatus
        {
            RESERVED,
            CONFIRMED,
            CANCELLED
        }
        [Required]
        public ReservationStatus Status { get; set; }

        public Reservation(ReservationID reservationID, CustomerID customerID, DateTime checkInDate, int numberOfNights, ReservationStatus status)
        {
            ReservationID = reservationID;
            CustomerID = customerID;
            CheckInDate = checkInDate;
            NumberOfNights = numberOfNights;
            Status = status;
        }
    }

}
