﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.Reservation
{
    public interface ReservationService
    {
        public ActionResult<IEnumerable<Reservation>> GetReservations();
        public ActionResult<Reservation> GetReservationById(Guid id);
        public ActionResult<Reservation> CreateReservation([FromBody] Reservation reservation);
        public IActionResult UpdateReservation(Guid id, [FromBody] Reservation updatedReservation);
        public IActionResult DeleteReservation(Guid id);
    }
}
