﻿using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.Attribute
{
    public class CustomDateTimeAttribute : RangeAttribute
    {
        public CustomDateTimeAttribute()
          : base(typeof(DateTime),
                  DateTime.Now.ToShortDateString(),
                  DateTime.Now.AddYears(1000).ToShortDateString())
        { }
    }
}
