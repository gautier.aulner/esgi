﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.RoomAssignment
{
    public interface RoomAssignmentService
    {
        public ActionResult<IEnumerable<RoomAssignement.RoomAssignment>> GetRoomAssignments();
        public ActionResult<RoomAssignement.RoomAssignment> GetRoomAssignmentById(Guid id);
        public ActionResult<RoomAssignement.RoomAssignment> CreateRoomAssignment([FromBody] RoomAssignement.RoomAssignment RoomAssignment);
        //public IActionResult UpdateRoomAssignment(Guid id, [FromBody] RoomAssignement.RoomAssignment updatedRoomAssignment);
        public IActionResult DeleteRoomAssignment(Guid id);
    }
}
