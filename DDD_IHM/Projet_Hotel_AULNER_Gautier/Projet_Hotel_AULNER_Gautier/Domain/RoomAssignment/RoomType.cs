﻿using System.ComponentModel.DataAnnotations;
using static Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement.RoomAssignment;

namespace Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement
{
    public class RoomParameters
    {
        [Required]
        public int Price { get; set; }

        [Required]
        public List<string> Equipements { get; set; }

        private static readonly Dictionary<RoomTypes, List<string>> _EquipmentsByRoom = new()
        {
            { RoomTypes.STANDARD, new List<string>() {
                "Lit 1 place", "Wifi", "TV"
            } },
            { RoomTypes.SUPERIOR, new List<string>() {
                "Lit 2 places", "Wifi", "TV écran plat", "Minibar", "Climatiseur"
            } },
            { RoomTypes.SUIT, new List<string>() {
                "Lit 2 places", "Wifi", "TV écran plat", "Minibar", "Climatiseur", "Baignoire", "Terrasse"
            } }
        };

        private static readonly Dictionary<RoomTypes, int> _PricesByRoom = new()
        {
            { RoomTypes.STANDARD, 50 },
            { RoomTypes.SUPERIOR, 100 },
            { RoomTypes.SUIT, 200 }
        };

        public RoomParameters(int roomType)
        {


            Price = _PricesByRoom[(RoomTypes)roomType];
            Equipements = _EquipmentsByRoom[(RoomTypes)roomType];
        }
    }
}
