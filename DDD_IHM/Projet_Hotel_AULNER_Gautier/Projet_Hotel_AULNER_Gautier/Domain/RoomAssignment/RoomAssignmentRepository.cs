﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement
{
    public interface RoomAssignmentRepository
    {
        //public bool RemoveRoomAssignment(Guid id);
        public List<RoomAssignment> GetRoomAssignments();
        public RoomAssignment? GetRoomAssignmentById(Guid id);
        public void SaveRoomAssignments(List<RoomAssignment> RoomAssignments);
        //public bool UpdateRoomAssignment(Guid id, RoomAssignment RoomAssignment);

    }
}
