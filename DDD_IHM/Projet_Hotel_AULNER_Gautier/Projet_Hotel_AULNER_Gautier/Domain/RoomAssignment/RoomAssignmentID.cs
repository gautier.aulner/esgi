﻿using Projet_Hotel_AULNER_Gautier.Domain.Customer;
using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement
{
    public class RoomAssignmentID
    {
        public Guid Value { get; }

        public RoomAssignmentID(Guid value)
        {
            Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is RoomAssignmentID otherID)
            {
                return Value == otherID.Value;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }
}
