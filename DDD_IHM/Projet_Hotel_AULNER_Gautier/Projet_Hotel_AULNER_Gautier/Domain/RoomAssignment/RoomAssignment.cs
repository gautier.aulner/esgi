﻿using Projet_Hotel_AULNER_Gautier.Domain.Attribute;
using Projet_Hotel_AULNER_Gautier.Domain.Reservation;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement
{
    public class RoomAssignment
    {
        [Required]
        public RoomAssignmentID AssignmentID { get; set; }

        [Required]
        public ReservationID ReservationID { get; set; }

        //[Required]
        //[CustomDateTime]
        //public DateTime CheckInDate { get; set; }

        public enum RoomTypes
        {
            STANDARD,
            SUPERIOR,
            SUIT
        }
        [Required]
        public RoomTypes RoomType { get; set; }

        public RoomAssignment(RoomAssignmentID assignmentID, ReservationID reservationID, RoomTypes roomType)
        {
            AssignmentID = assignmentID;
            ReservationID = reservationID;
            //CheckInDate = checkInDate;
            RoomType = roomType;

        }
    }

}
