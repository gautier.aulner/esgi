﻿namespace Projet_Hotel_AULNER_Gautier.Domain.Customer
{
    public record LoginInformation
    {
        public string? Email { get; set; }
        public string? Password { get; set; }

    }
}
