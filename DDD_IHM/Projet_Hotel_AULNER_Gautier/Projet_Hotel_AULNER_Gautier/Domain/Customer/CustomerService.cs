﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.Customer
{
    public interface CustomerService
    {
        public ActionResult<IEnumerable<Customer>> GetCustomers();
        public ActionResult<Customer> GetCustomerById(Guid id);
        public ActionResult<Customer> CreateCustomer([FromBody] Customer customer);
        public IActionResult UpdateCustomer(Guid id, [FromBody] Customer updatedCustomer);
        public IActionResult DeleteCustomer(Guid id);
        public IActionResult Login([FromBody] LoginInformation loginInformation);
    }
}
