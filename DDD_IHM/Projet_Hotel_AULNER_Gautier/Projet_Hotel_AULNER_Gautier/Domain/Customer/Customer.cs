﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace Projet_Hotel_AULNER_Gautier.Domain.Customer
{
    public class Customer
    {

        public CustomerID CustomerID { get; set; }

        [Required]
        [StringLength(100)]
        [NotNull]
        public string FullName { get; set; }

        [Required]
        [StringLength(100)]
        [NotNull]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [Phone]
        public string PhoneNumber { get; set; }

        [Required]
        public Wallet Wallet { get; set; }



        public Customer(CustomerID customerID, string fullName, string password, string email, string phoneNumber, Wallet wallet)
        {
            CustomerID = customerID;
            FullName = fullName;
            Password = password;
            Email = email;
            PhoneNumber = phoneNumber;
            Wallet = wallet;
        }
    }
}
