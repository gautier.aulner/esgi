﻿using Microsoft.AspNetCore.Mvc;

namespace Projet_Hotel_AULNER_Gautier.Domain.Customer
{
    public interface CustomerRepository
    {
        public bool RemoveCustomer(Guid id);
        public List<Customer> GetCustomers();
        public Customer? GetCustomerById(Guid id);
        public void SaveCustomers(List<Customer> customers);
        public bool UpdateCustomer(Guid id, Customer customer);



    }
}
