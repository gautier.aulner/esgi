﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Projet_Hotel_AULNER_Gautier.Domain.Customer
{
    public class Wallet
    {
        [Required]
        [Range(0, int.MaxValue)]
        public decimal Balance { get; set; }

        public enum ISO2Currency
        {
            EUR, USD, GBP, JPY, CHF
        }
        [Required]
        public ISO2Currency Currency { get; set; }

        public Wallet()
        {

        }

        public Wallet(decimal balance, ISO2Currency currency)
        {
            Balance = balance;
            Currency = currency;
        }
    }
}
