﻿using Microsoft.EntityFrameworkCore;
using Projet_Hotel_AULNER_Gautier.Domain.Customer;

namespace Projet_Hotel_AULNER_Gautier
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(p => p.CustomerID);

        }


    }
}
