﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Projet_Hotel_AULNER_Gautier.Domain.Reservation;
using Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement;
using System.Text;

namespace Projet_Hotel_AULNER_Gautier.Repository
{
    public class RoomAssignmentDatabase : RoomAssignmentRepository
    {
        private readonly string jsonFilePath = Path.Combine(Directory.GetCurrentDirectory(), "JSONDatabase", "RoomAssignment.json");
        public RoomAssignmentDatabase() { }

        public void AddRoomAssignment(RoomAssignment roomAssignment)
        {
            var roomAssignments = GetRoomAssignments();
            roomAssignments.Add(roomAssignment);
            SaveRoomAssignments(roomAssignments);
            var updatedJson = JsonConvert.SerializeObject(roomAssignments, Formatting.Indented);
            System.IO.File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }


        public void SaveRoomAssignments(List<RoomAssignment> roomAssignment)
        {
            var updatedJson = JsonConvert.SerializeObject(roomAssignment, Formatting.Indented);
            File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public RoomAssignment? GetRoomAssignmentById(Guid id)
        {
            var roomAssignments = GetRoomAssignments();
            var roomAssignment = roomAssignments.FirstOrDefault((r) =>
            {
                if (r.ReservationID == null)
                {
                    return false;
                }
                return r.ReservationID.Value == id;
            });
            return roomAssignment;
        }

        public List<RoomAssignment> GetRoomAssignments()
        {
            var json = File.ReadAllText(jsonFilePath);
            return JsonConvert.DeserializeObject<List<RoomAssignment>>(json) ?? new List<RoomAssignment>();
        }

        public bool RemoveRoomAssignment(Guid id)
        {
            var roomAssignments = GetRoomAssignments();
            var existingRoomAssignment = roomAssignments.FirstOrDefault((r) =>
            {
                if (r.ReservationID == null)
                {
                    return false;
                }
                return r.ReservationID.Value == id;
            });

            if (existingRoomAssignment == null)
            {
                return false;
            }

            //Reservations0

            var newReservations = roomAssignments.Remove(existingRoomAssignment);
            //List<Reservation>.IndexOf(Reservations);

            SaveRoomAssignments(roomAssignments);
            return true;
        }


    }
}
