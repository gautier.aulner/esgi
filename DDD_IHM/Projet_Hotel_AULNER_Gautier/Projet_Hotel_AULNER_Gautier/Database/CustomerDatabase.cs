﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Projet_Hotel_AULNER_Gautier.Domain.Customer;
using System.Text;

namespace Projet_Hotel_AULNER_Gautier.Repository
{
    public class CustomerDatabase : CustomerRepository
    {
        private readonly string jsonFilePath = Path.Combine(Directory.GetCurrentDirectory(), "JSONDatabase", "Customers.json");
        public CustomerDatabase() { }

        public void AddCustomer(Customer customer)
        {
            var customers = GetCustomers();
            customers.Add(customer);
            SaveCustomers(customers);
            var updatedJson = JsonConvert.SerializeObject(customers, Formatting.Indented);
            System.IO.File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }


        public void SaveCustomers(List<Customer> customers)
        {
            var updatedJson = JsonConvert.SerializeObject(customers, Formatting.Indented);
            File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public Customer? GetCustomerById(Guid id)
        {
            var customers = GetCustomers();
            var customer = customers.FirstOrDefault((c) =>
            {
                if (c.CustomerID == null)
                {
                    return false;
                }
                return c.CustomerID.Value == id;
            });
            return customer;
        }

        public List<Customer> GetCustomers()
        {
            var json = File.ReadAllText(jsonFilePath);
            return JsonConvert.DeserializeObject<List<Customer>>(json) ?? new List<Customer>();
        }

        public bool RemoveCustomer(Guid id)
        {
            var customers = GetCustomers();
            var existingCustomer = customers.FirstOrDefault((c) =>
            {
                if (c.CustomerID == null)
                {
                    return false;
                }
                return c.CustomerID.Value == id;
            });

            if (existingCustomer == null)
            {
                return false;
            }

            //customers0

            var newCustomers = customers.Remove(existingCustomer);
            //List<Customer>.IndexOf(customers);

            SaveCustomers(customers);
            return true;
        }

        public bool UpdateCustomer(Guid id, Customer customer)
        {
            bool userFound = false;
            List<Customer> customers = GetCustomers();

            for (int i = 0; i < customers.Count; i++)
            {
                if (customers[i].CustomerID.Value == id)
                {
                    userFound = true;

                    customers[i].FullName = customer.FullName;
                    customers[i].Email = customer.Email;
                    customers[i].PhoneNumber = customer.PhoneNumber;
                    customers[i].Wallet.Balance = customer.Wallet.Balance;
                    customers[i].Wallet.Currency = customer.Wallet.Currency;
                }
            }

            //var existingCustomer = GetCustomerById(customer.CustomerID.Value);

            //if (existingCustomer == null)
            //{
            //    return false;
            //}
            SaveCustomers(customers);
            return userFound;
        }
    }
}
