﻿using Microsoft.AspNetCore.Mvc;
using Projet_Hotel_AULNER_Gautier.Domain.Reservation;
using Projet_Hotel_AULNER_Gautier.Domain.Reservation;
using Projet_Hotel_AULNER_Gautier.Repository;

namespace Projet_Hotel_AULNER_Gautier.Services
{
    [ApiController]
    [Route("api/reservation")]
    public class ReservationAPI : ControllerBase, ReservationService
    {
        private readonly ILogger<ReservationAPI> _logger;
        private readonly ReservationDatabase _context;


        public ReservationAPI(ILogger<ReservationAPI> logger)
        {
            _logger = logger;
            _context = new ReservationDatabase();
        }

        // GET api/Reservation
        [HttpGet]
        public ActionResult<IEnumerable<Reservation>> GetReservations()
        {
            return Ok(_context.GetReservations());
        }

        // GET api/Reservation/5
        [HttpGet("{id}")]
        public ActionResult<Reservation> GetReservationById(Guid id)
        {
            Reservation? reservation = _context.GetReservationById(id);
            if (reservation == null)
            {
                return BadRequest("Reservation not found.");
            }
            return Ok(reservation);
        }

        // POST api/Reservation
        [HttpPost]
        public ActionResult<Reservation> CreateReservation([FromBody] Reservation reservation)
        {
            reservation.ReservationID = new(Guid.NewGuid());
            _context.AddReservation(reservation);
            return CreatedAtAction(nameof(GetReservationById), new { id = reservation.ReservationID.Value }, reservation);
        }

        // PUT api/Reservation/5
        [HttpPut("{id}")]
        public IActionResult UpdateReservation(Guid id, [FromBody] Reservation updatedReservation)
        {
            if (_context.UpdateReservation(id, updatedReservation))
            {
                return NoContent();
            }
            return NotFound();
        }

        // DELETE api/Reservation/5
        [HttpDelete("{id}")]
        public IActionResult DeleteReservation(Guid id)
        {
            if (_context.RemoveReservation(id))
            {
                return NoContent();
            }
            return BadRequest();
        }
    }
}
