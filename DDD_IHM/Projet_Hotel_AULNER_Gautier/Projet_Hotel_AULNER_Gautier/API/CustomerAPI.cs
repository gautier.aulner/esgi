﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Projet_Hotel_AULNER_Gautier.Domain.Customer;
using Projet_Hotel_AULNER_Gautier.Repository;
using System.Text;

namespace Projet_Hotel_AULNER_Gautier.Services
{
    [ApiController]
    [Route("api/customer")]
    public class CustomerAPI : ControllerBase, CustomerService
    {
        private readonly ILogger<CustomerAPI> _logger;
        private readonly CustomerDatabase _context;


        public CustomerAPI(ILogger<CustomerAPI> logger)
        {
            _logger = logger;
            _context = new CustomerDatabase();

        }

        // GET api/customer
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> GetCustomers()
        {
            return Ok(_context.GetCustomers());
        }

        // GET api/customer/5
        [HttpGet("{id}")]
        public ActionResult<Customer> GetCustomerById(Guid id)
        {
            Customer? customer = _context.GetCustomerById(id);
            if (customer == null)
            {
                return BadRequest("Customer not found.");
            }
            return Ok(customer);
        }

        // POST api/customer
        [HttpPost]
        public ActionResult<Customer> CreateCustomer([FromBody] Customer customer)
        {
            customer.CustomerID = new(Guid.NewGuid());
            _context.AddCustomer(customer);
            return CreatedAtAction(nameof(GetCustomerById), new { id = customer.CustomerID.Value }, customer);
        }

        // PUT api/customer/5
        [HttpPut("{id}")]
        public IActionResult UpdateCustomer(Guid id, [FromBody] Customer updatedCustomer)
        {
            if (_context.UpdateCustomer(id, updatedCustomer))
            {
                return NoContent();
            }
            return NotFound();
        }

        // DELETE api/customer/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCustomer(Guid id)
        {
            if (_context.RemoveCustomer(id))
            {
                return NoContent();
            }
            return BadRequest();
        }


        // POST api/customer/login
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginInformation loginInformation)

        {
            var customers = _context.GetCustomers();
            var existingCustomer = customers.FirstOrDefault(c => c.Email == loginInformation.Email && c.Password == loginInformation.Password);

            if (existingCustomer == null)
            {
                return Forbid();
            }
            return Ok(existingCustomer);
        }
    }
}
