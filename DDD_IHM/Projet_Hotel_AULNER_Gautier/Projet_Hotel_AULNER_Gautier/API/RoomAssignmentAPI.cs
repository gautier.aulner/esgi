﻿using Microsoft.AspNetCore.Mvc;

using Projet_Hotel_AULNER_Gautier.Domain.RoomAssignement;
using Projet_Hotel_AULNER_Gautier.Domain.RoomAssignment;
using Projet_Hotel_AULNER_Gautier.Repository;

namespace Projet_Hotel_AULNER_Gautier.Services
{
    [ApiController]
    [Route("api/room-assignment")]
    public class RoomAssignmentAPI : ControllerBase, RoomAssignmentService
    {
        private readonly ILogger<RoomAssignmentAPI> _logger;
        private readonly RoomAssignmentDatabase _context;


        public RoomAssignmentAPI(ILogger<RoomAssignmentAPI> logger)
        {
            _logger = logger;
            _context = new RoomAssignmentDatabase();
        }

        // GET api/RoomAssignment
        [HttpGet]
        public ActionResult<IEnumerable<RoomAssignment>> GetRoomAssignments()
        {
            return Ok(_context.GetRoomAssignments());
        }

        // GET api/RoomAssignment/5
        [HttpGet("{id}")]
        public ActionResult<RoomAssignment> GetRoomAssignmentById(Guid id)
        {
            RoomAssignment? roomAssignment = _context.GetRoomAssignmentById(id);
            if (roomAssignment == null)
            {
                return BadRequest("RoomAssignment not found.");
            }
            return Ok(roomAssignment);
        }

        // POST api/RoomAssignment
        [HttpPost]
        public ActionResult<RoomAssignment> CreateRoomAssignment([FromBody] RoomAssignment roomAssignment)
        {
            roomAssignment.AssignmentID = new(Guid.NewGuid());
            _context.AddRoomAssignment(roomAssignment);
            return CreatedAtAction(nameof(GetRoomAssignmentById), new { id = roomAssignment.AssignmentID.Value }, roomAssignment);
        }

        //// PUT api/RoomAssignment/5
        //[HttpPut("{id}")]
        //public IActionResult UpdateRoomAssignment(Guid id, [FromBody] RoomAssignment updatedRoomAssignment)
        //{
        //    if (_context.UpdateRoomAssignment(id, updatedRoomAssignment))
        //    {
        //        return NoContent();
        //    }
        //    return NotFound();
        //}

        // DELETE api/RoomAssignment/5
        [HttpDelete("{id}")]
        public IActionResult DeleteRoomAssignment(Guid id)
        {
            if (_context.RemoveRoomAssignment(id))
            {
                return NoContent();
            }
            return BadRequest();
        }
    }
}
