﻿using Microsoft.EntityFrameworkCore;
using csharp_tokio_hotel.Domain.Customer;

namespace csharp_tokio_hotel
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(p => p.CustomerID);

        }


    }
}
