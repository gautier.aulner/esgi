﻿using System.ComponentModel.DataAnnotations;

namespace csharp_tokio_hotel.Domain.Customer
{
    public class CustomerID
    {
        public Guid Value { get; set; }


        public CustomerID(Guid value)
        {
            Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is CustomerID otherID)
            {
                return Value == otherID.Value;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }
    }

}
