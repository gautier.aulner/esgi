﻿using System.Collections.Generic;
using static csharp_tokio_hotel.Domain.Reservation.Reservation;

namespace csharp_tokio_hotel.Domain.Customer
{
    public static class ISO2Currency
    {
        public enum ISO2Currencies
        {
            EUR, USD, GBP, JPY, CHF
        }

        public static Dictionary<ISO2Currencies, double> ChangeAmount = new()
        {
            {ISO2Currencies.EUR, 1},
            {ISO2Currencies.USD, 1.08},
            {ISO2Currencies.GBP, 1.17},
            {ISO2Currencies.JPY, 156},
            {ISO2Currencies.CHF, 1.06},
        };


        private static readonly Dictionary<RoomTypes, List<string>> _EquipmentsByRoom = new()
        {
            { RoomTypes.STANDARD, new List<string>() {
                "Lit 1 place", "Wifi", "TV"
            } },
            { RoomTypes.SUPERIOR, new List<string>() {
                "Lit 2 places", "Wifi", "TV écran plat", "Minibar", "Climatiseur"
            } },
            { RoomTypes.SUIT, new List<string>() {
                "Lit 2 places", "Wifi", "TV écran plat", "Minibar", "Climatiseur", "Baignoire", "Terrasse"
            } }
        };
    }
}
