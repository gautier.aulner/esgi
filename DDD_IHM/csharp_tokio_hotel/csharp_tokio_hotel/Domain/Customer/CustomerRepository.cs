﻿using Microsoft.AspNetCore.Mvc;

namespace csharp_tokio_hotel.Domain.Customer
{
    public interface CustomerRepository
    {
        public bool RemoveCustomer(Guid id);
        public List<Customer> GetCustomers();
        public Customer? GetCustomerById(Guid id);
        public void SaveCustomers(List<Customer> customers);
        public bool UpdateCustomer(Guid id, Customer customer);

        public void AddCustomer(Customer customer);


    }
}
