﻿using Microsoft.AspNetCore.Mvc;

namespace csharp_tokio_hotel.Domain.Customer
{
    public interface CustomerServiceInt
    {
        public ActionResult<IEnumerable<Customer>> GetCustomers();
        public ActionResult<Customer> GetCustomerById(Guid id);
        public ActionResult<Customer> CreateCustomer([FromBody] Customer customer);
        public IActionResult UpdateCustomer(Guid id, [FromBody] Customer updatedCustomer);
        public IActionResult DeleteCustomer(Guid id);
        public IActionResult Login([FromBody] LoginInformation loginInformation);

        public IActionResult UpdateWallet(Guid id, [FromBody] Wallet wallet);

    }
}
