﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using static csharp_tokio_hotel.Domain.Customer.ISO2Currency;

namespace csharp_tokio_hotel.Domain.Customer
{
    public class Wallet
    {
        [Required]
        [Range(0, int.MaxValue)]
        public decimal Balance { get; set; }


        [Required]
        public ISO2Currencies Currency { get; set; }

        public Wallet()
        {

        }

        public Wallet(decimal balance, ISO2Currencies currency)
        {
            Balance = balance;
            Currency = currency;
        }


    }
}
