﻿namespace csharp_tokio_hotel.Domain.Customer
{
    public record LoginInformation
    {
        public string? Email { get; set; }
        public string? Password { get; set; }

    }
}
