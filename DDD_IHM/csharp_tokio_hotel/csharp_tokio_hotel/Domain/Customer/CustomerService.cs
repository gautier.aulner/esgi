﻿using csharp_tokio_hotel.Repository;
using csharp_tokio_hotel.Services;
using Microsoft.AspNetCore.Mvc;
using static csharp_tokio_hotel.Domain.Customer.ISO2Currency;

namespace csharp_tokio_hotel.Domain.Customer
{
    public class CustomerService
    {
        private readonly CustomerRepository _context;

        public CustomerService(CustomerRepository repository)
        {
            _context = repository;
        }
        public List<Customer> GetCustomers()
        {
            return _context.GetCustomers();
        }
        public Customer? GetCustomerById(Guid id)
        {
            return _context.GetCustomerById(id);
        }
        public Customer CreateCustomer(Customer customer)
        {
            customer.CustomerID = new(Guid.NewGuid());
            customer.Wallet = new Wallet(0, ISO2Currencies.EUR);
            _context.AddCustomer(customer);
            return customer;
        }
        public bool UpdateCustomer(Guid id, Customer updatedCustomer)
        {
            return _context.UpdateCustomer(id, updatedCustomer);
        }
        public bool DeleteCustomer(Guid id)
        {
            return _context.RemoveCustomer(id);
        }

        public Customer? GetCustomerByLogin(LoginInformation loginInformation)
        {
            var customers = _context.GetCustomers();
            var existingCustomer = customers.FirstOrDefault(c => c.Email == loginInformation.Email && c.Password == loginInformation.Password);

            return existingCustomer;
        }

        public bool CheckWalletAmount(Guid customerId, int amountToCheck)
        {
            Customer? customer = _context.GetCustomerById(customerId);

            if (customer == null)
                return false;
            if (customer.Wallet == null)
                return false;

            if(customer.Wallet.Balance < amountToCheck) 
                return false;

            return true;
        }

        public bool UpdateWallet(Guid id, Wallet wallet)
        {
            Customer? customer = _context.GetCustomerById(id);

            if (customer == null)
                return false;
            if (customer.Wallet == null)
                return false;

            wallet = AdaptCurrency(customer.Wallet.Balance, wallet, wallet.Currency);


            customer.Wallet = wallet;

            return _context.UpdateCustomer(id, customer);
        }

        public bool UpdateWalletByAmountEuro(Guid id, int amountEuro)
        {
            Customer? customer = _context.GetCustomerById(id);

            if (customer == null)
                return false;
            if (customer.Wallet == null)
                return false;

            customer.Wallet = new Wallet(customer.Wallet.Balance - amountEuro, ISO2Currencies.EUR);

            return _context.UpdateCustomer(id, customer);
        }

        public Wallet AdaptCurrency(decimal balance, Wallet walletToUpdated, ISO2Currencies currency)
        {
            walletToUpdated.Balance = balance + walletToUpdated.Balance * (decimal)ChangeAmount[currency];
            walletToUpdated.Currency = ISO2Currencies.EUR;
            return walletToUpdated;
        }

    }
}
