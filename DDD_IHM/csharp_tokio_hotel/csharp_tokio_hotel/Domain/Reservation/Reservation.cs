﻿using csharp_tokio_hotel.Domain.Attribute;
using csharp_tokio_hotel.Domain.Customer;
using System.ComponentModel.DataAnnotations;

namespace csharp_tokio_hotel.Domain.Reservation
{
    public class Reservation
    {
        //[Required]
        public ReservationID? ReservationID { get; set; }

        [Required]
        public CustomerID CustomerID { get; set; }

        [Required]
        [CustomDateTime]
        public DateTime CheckInDate { get; set; }

        public DateTime CreatedAt { get; set; }


        [Required]
        [Range(0, int.MaxValue)]
        public int NumberOfNights { get; set; }

        public enum ReservationStatus
        {
            RESERVED,
            CONFIRMED,
            CANCELLED
        }

        public ReservationStatus Status { get; set; }

        public enum RoomTypes
        {
            STANDARD,
            SUPERIOR,
            SUIT
        }

        [Required]
        public RoomTypes RoomType { get; set; }


        public Reservation(ReservationID reservationID, CustomerID customerID, DateTime checkInDate, int numberOfNights, ReservationStatus status, DateTime createdAt, RoomTypes roomType)
        {
            ReservationID = reservationID;
            CustomerID = customerID;
            CheckInDate = checkInDate;
            NumberOfNights = numberOfNights;
            Status = status;
            CreatedAt = createdAt;
            RoomType = roomType;

        }
    }

}
