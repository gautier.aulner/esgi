﻿using static csharp_tokio_hotel.Domain.Reservation.Reservation;

namespace csharp_tokio_hotel.Domain.Reservation
{
    public class ReservationService
    {
        private readonly ReservationRepository _context;



        public ReservationService(ReservationRepository repository)
        {
            _context = repository;
        }

        public List<Reservation> GetReservations()
        {
            return _context.GetReservations();

        }
        public Reservation? GetReservationById(Guid id)
        {
            return _context.GetReservationById(id);
        }
        public List<Reservation> GetReservationsByCustomerId(Guid id)
        {
            return _context.GetReservationsByCustomerId(id);
        }

        public Reservation CreateReservation(Reservation reservation)
        {
            // Completing the reservation input with values
            reservation.ReservationID = new(Guid.NewGuid());
            reservation.Status = ReservationStatus.RESERVED;
            reservation.CreatedAt = DateTime.Now;

            // Creating the room assignment
            //RoomAssignement.RoomAssignment roomAssignment = new(
            //    new(Guid.NewGuid()),
            //    reservationInput.Reservation.ReservationID
            //    //reservationInput.RoomType
            //);

            // Saving into DB
            _context.AddReservation(reservation);
            //_roomAssignementService.CreateRoomAssignment(roomAssignment);

            return reservation;
        }
        public bool UpdateReservation(Guid id, Reservation updatedReservation)
        {
            return _context.UpdateReservation(id, updatedReservation);

        }
        public bool DeleteReservation(Guid id)
        {
            return _context.RemoveReservation(id);
        }
    }
}
