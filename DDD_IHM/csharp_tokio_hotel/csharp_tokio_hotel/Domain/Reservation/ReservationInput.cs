﻿using static csharp_tokio_hotel.Domain.Reservation.Reservation;

namespace csharp_tokio_hotel.Domain.Reservation
{
    public class ReservationInput
    {
        public Reservation Reservation { get; set; }
        public RoomTypes RoomType { get; set; }

        public ReservationInput(Reservation reservation, RoomTypes roomType)
        {
            Reservation = reservation;
            RoomType = roomType;
        }

    }
}
