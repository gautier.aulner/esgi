﻿using Microsoft.AspNetCore.Mvc;

namespace csharp_tokio_hotel.Domain.Reservation
{
    public interface ReservationRepository
    {
        public bool RemoveReservation(Guid id);
        public List<Reservation> GetReservations();
        public Reservation? GetReservationById(Guid id);
        public List<Reservation> GetReservationsByCustomerId(Guid id);
        public void SaveReservations(List<Reservation> reservations);
        public void AddReservation(Reservation reservation);

        public bool UpdateReservation(Guid id, Reservation reservation);

    }
}
