﻿using System.ComponentModel.DataAnnotations;

namespace csharp_tokio_hotel.Domain.Attribute
{
    public class CustomDateTimeAttribute : RangeAttribute
    {
        public CustomDateTimeAttribute()
          : base(typeof(DateTime),
                  DateTime.Now.ToShortDateString(),
                  DateTime.Now.AddYears(1000).ToShortDateString())
        { }
    }
}
