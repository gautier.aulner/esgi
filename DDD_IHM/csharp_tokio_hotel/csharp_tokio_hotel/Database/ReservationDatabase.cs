﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using csharp_tokio_hotel.Domain.Reservation;
using System.Text;

namespace csharp_tokio_hotel.Repository
{
    public class ReservationDatabase : ReservationRepository
    {
        private readonly string jsonFilePath = Path.Combine(Directory.GetCurrentDirectory(), "JSONDatabase", "Reservations.json");
        public ReservationDatabase() { }

        public void AddReservation(Reservation reservation)
        {
            var reservations = GetReservations();
            reservations.Add(reservation);
            SaveReservations(reservations);
            var updatedJson = JsonConvert.SerializeObject(reservations, Formatting.Indented);
            System.IO.File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }


        public void SaveReservations(List<Reservation> reservation)
        {
            var updatedJson = JsonConvert.SerializeObject(reservation, Formatting.Indented);
            File.WriteAllText(jsonFilePath, updatedJson, Encoding.UTF8);
        }

        /// <summary>
        ///     
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public Reservation? GetReservationById(Guid id)
        {
            var reservations = GetReservations();
            var reservation = reservations.FirstOrDefault((r) =>
            {
                if (r.ReservationID == null)
                {
                    return false;
                }
                return r.ReservationID.Value == id;
            });
            return reservation;
        }

        public List<Reservation> GetReservations()
        {
            var json = File.ReadAllText(jsonFilePath);
            return JsonConvert.DeserializeObject<List<Reservation>>(json) ?? new List<Reservation>();
        }

        public List<Reservation> GetReservationsByCustomerId(Guid id)
        {
            var reservations = GetReservations();
            var reservation = reservations.FindAll((r) =>
            {
                if (r.CustomerID == null)
                {
                    return false;
                }
                return r.CustomerID.Value == id;
            });
            return reservation;
        }


        public bool RemoveReservation(Guid id)
        {
            var reservations = GetReservations();
            var existingReservation = reservations.FirstOrDefault((r) =>
            {
                if (r.ReservationID == null)
                {
                    return false;
                }
                return r.ReservationID.Value == id;
            });

            if (existingReservation == null)
            {
                return false;
            }

            //Reservations0

            var newReservations = reservations.Remove(existingReservation);
            //List<Reservation>.IndexOf(Reservations);

            SaveReservations(reservations);
            return true;
        }

        public bool UpdateReservation(Guid id, Reservation reservation)
        {
            bool userFound = false;
            List<Reservation> reservations = GetReservations();

            for (int i = 0; i < reservations.Count; i++)
            {
                if (reservations[i].ReservationID.Value == id)
                {
                    userFound = true;

                    reservations[i].Status = reservation.Status;
                }
            }

            //var existingReservation = GetReservationById(Reservation.ReservationID.Value);

            //if (existingReservation == null)
            //{
            //    return false;
            //}
            SaveReservations(reservations);
            return userFound;
        }
    }
}
