﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using csharp_tokio_hotel.Domain.Customer;
using csharp_tokio_hotel.Repository;
using System.Text;
using System.Net;
using static csharp_tokio_hotel.Domain.Customer.Wallet;
using static csharp_tokio_hotel.Domain.Customer.ISO2Currency;
using System.Collections.Generic;

namespace csharp_tokio_hotel.Services
{
    [ApiController]
    [Route("api/customer")]
    public class CustomerAPI : ControllerBase
    {
        private readonly ILogger<CustomerAPI> _logger;
        private readonly CustomerService _service;


        public CustomerAPI(ILogger<CustomerAPI> logger)
        {
            _logger = logger;
            _service = new CustomerService(new CustomerDatabase());
        }

        // GET api/customer
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> GetCustomers()
        {
            List<Customer> customers = _service.GetCustomers();
            if (customers != null && customers.Count > 0)
            {
                return Ok(customers);
            }
            return NotFound();
        }

        // GET api/customer/5
        [HttpGet("{id}")]
        public ActionResult<Customer> GetCustomerById(Guid id)
        {
            Customer? customer = _service.GetCustomerById(id);
            if (customer == null)
            {
                return NotFound("Customer not found.");
            }
            return Ok(customer);
        }

        // POST api/customer
        [HttpPost]
        public ActionResult<Customer> CreateCustomer([FromBody] Customer customer)
        {
            customer = _service.CreateCustomer(customer);
            return CreatedAtAction(nameof(GetCustomerById), new { id = customer.CustomerID.Value }, customer);
        }

        // PUT api/customer/5
        [HttpPut("{id}")]
        public IActionResult UpdateCustomer(Guid id, [FromBody] Customer updatedCustomer)
        {
            if (_service.UpdateCustomer(id, updatedCustomer))
            {
                return NoContent();
            }
            return NotFound();
        }

        // DELETE api/customer/5
        [HttpDelete("{id}")]
        public IActionResult DeleteCustomer(Guid id)
        {
            if (_service.DeleteCustomer(id))
            {
                return NoContent();
            }
            return BadRequest();
        }


        // POST api/customer/login
        [HttpPost("login")]
        public IActionResult Login([FromBody] LoginInformation loginInformation)
        {
            Customer? customer = _service.GetCustomerByLogin(loginInformation);
            if (customer == null)
            {
                return Unauthorized();
            }
            return Ok(customer);
        }

        // PUT api/customer/wallet/5
        [HttpPut("{id}/wallet")]
        public IActionResult UpdateWallet(Guid id, [FromBody] Wallet wallet)
        {
            if (_service.UpdateWallet(id, wallet))
            {
                return Ok();
            }
            return BadRequest();
        }

        // GET api/customer/wallet/check
        [HttpGet("{id}/wallet/check")]
        public IActionResult CheckWalletAmount(Guid id, [FromQuery] int amountToCheck)
        {
            if (_service.CheckWalletAmount(id, amountToCheck))
            {
                return Ok();
            }
            return StatusCode(403);
        }
    }
}
