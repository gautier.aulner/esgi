﻿using Microsoft.AspNetCore.Mvc;
using csharp_tokio_hotel.Domain.Reservation;
using csharp_tokio_hotel.Repository;
using csharp_tokio_hotel.Domain.Customer;
using System.Linq;
using static csharp_tokio_hotel.Domain.Reservation.Reservation;

namespace csharp_tokio_hotel.Services
{
    [ApiController]
    [Route("api/reservation")]
    public class ReservationAPI : ControllerBase
    {
        private readonly ILogger<ReservationAPI> _logger;
        private readonly ReservationService _service;
        private readonly CustomerService _customerService;


        public ReservationAPI(ILogger<ReservationAPI> logger)
        {
            _logger = logger;
            _service = new ReservationService(new ReservationDatabase());
            _customerService = new CustomerService(new CustomerDatabase());
        }

        // GET api/Reservation
        [HttpGet]
        public ActionResult<IEnumerable<Reservation>> GetReservations()
        {
            List<Reservation> reservations = _service.GetReservations();
            if (reservations != null && reservations.Count > 0)
            {
                return Ok(reservations);
            }
            return NotFound();
        }

        // GET api/Reservation/5
        [HttpGet("{id}")]
        public ActionResult<Reservation> GetReservationById(Guid id)
        {
            Reservation? reservation = _service.GetReservationById(id);
            if (reservation == null)
            {
                return NotFound("Reservation not found.");
            }
            return Ok(reservation);
        }

        // GET api/reservation/customer/{id}
        [HttpGet("customer/{id}")]
        public ActionResult<List<Reservation>> GetReservationsByCustomerId(Guid id)
        {
            List<Reservation> reservations = _service.GetReservationsByCustomerId(id);
            if (reservations == null)
            {
                return NotFound("Reservations not found.");
            }

            //List<EnhancedReservation> enhancedReservations = _roomAssignmentService.GetRoomAssignmentById(re)

            return Ok(reservations);
        }

        // POST api/Reservation
        [HttpPost]
        public ActionResult<Reservation> CreateReservation([FromBody] Reservation reservation)
        {
            if (reservation == null)
                return BadRequest();

            // If not enough amount, reject the reservations
            if (!_customerService.CheckWalletAmount(reservation.CustomerID.Value, new RoomParameters(reservation.RoomType).Price))
                return StatusCode(403);

            _service.CreateReservation(reservation);

            return CreatedAtAction(nameof(CreateReservation), new { id = reservation.ReservationID.Value }, reservation);
        }

        // POST api/reservation/list
        [HttpPost("/api/reservation/list")]
        public IActionResult CreateMultipleReservation([FromBody] List<Reservation> reservations)
        {
            if (reservations.Count == 0)
                return BadRequest();

            int totalAmount = reservations.Aggregate(0, (total, reservation) => total + new RoomParameters(reservation.RoomType).Price) / 2;

            // If not enough amount, reject the reservations
            if (!_customerService.CheckWalletAmount(reservations[0].CustomerID.Value, totalAmount))
                return StatusCode(403);


            for (int i = 0; i < reservations.Count; i++)
            {
                // Making the reservation
                _service.CreateReservation(reservations[i]);

                // Updating the wallet by substracting the current room price divided by 2
                // (1/2 for the reservation and 1/2 for the confirmation)
                _customerService.UpdateWalletByAmountEuro(
                    reservations[i].CustomerID.Value, 
                    new RoomParameters(reservations[i].RoomType).Price / 2
                );
            }
            return StatusCode(201);
        }

        // PUT api/Reservation/5
        [HttpPut("{id}")]
        public IActionResult UpdateReservation(Guid id, [FromBody] Reservation updatedReservation)
        {
            if (_service.UpdateReservation(id, updatedReservation))
            {
                return NoContent();
            }
            return NotFound();
        }

        // PUT api/Reservation/5/status?status={status}
        [HttpPut("{id}/status")]
        public IActionResult UpdateReservationStatus(Guid id, [FromQuery] ReservationStatus status)
        {
            Reservation? reservation = _service.GetReservationById(id);

            if (reservation == null)
                return BadRequest();

            reservation.Status = status;
            if (_service.UpdateReservation(id, reservation))
            {
                if(reservation.Status == ReservationStatus.CONFIRMED)
                {
                    if(!_customerService.UpdateWalletByAmountEuro(
                        reservation.CustomerID.Value,
                        new RoomParameters(reservation.RoomType).Price / 2
                    ))
                    {
                        return BadRequest();
                    }
                }
                return Ok();
            }
            return NotFound();
        }

        // DELETE api/Reservation/5
        [HttpDelete("{id}")]
        public IActionResult DeleteReservation(Guid id)
        {
            if (_service.DeleteReservation(id))
            {
                return NoContent();
            }
            return BadRequest();
        }
    }
}
