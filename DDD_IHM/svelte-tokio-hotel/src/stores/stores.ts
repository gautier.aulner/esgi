import { writable } from 'svelte/store';

export const isSmallScreen = writable(false);
export const customerID = writable(0);
export const isVisitor = writable(false);

export const updateWalletStore = writable(false);
