process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
/** @type {import('./$types').PageServerLoad} */
export async function load({ params }) {
	// if(sessionStorage.getItem('customerID')){
	//     return {
	//         post: await getUser(sessionStorage.getItem('customerID'));
	//     };
	// }
	// return {
	//     post: null
	// };
}

const getUser = async (customerID: string) => {
	let user = null;
	await fetch('https://localhost:7067/api/customer/' + customerID, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			await response.json().then((data) => {
				user = data;
			});
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});
	return user;
};
