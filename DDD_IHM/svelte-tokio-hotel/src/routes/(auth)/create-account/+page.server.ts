/** @type {import('./$types').Actions} */
export const actions = {
	default: async ({ cookies, request }) => {
		const data = await request.formData();

		const requestBody = JSON.stringify({
			email: data.get('email'),
			password: data.get('password'),
			fullname: data.get('fullname'),
			phoneNumber: data.get('phonenumber')
		});

		console.log('requestBody', requestBody);
		let requestOk = false;
		await fetch('https://localhost:7067/api/customer', {
			method: 'POST',
			body: requestBody,
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(async (response) => {
				console.log('response', response.status);
				requestOk = response.status === 201;
			})
			.catch((error) => {
				console.log('error', error);
				return [];
			});

		return { success: requestOk };
	}
};
