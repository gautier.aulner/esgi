/** @type {import('./$types').Actions} */
export const actions = {
	default: async ({ cookies, request }) => {
		const data = await request.formData();
		const email = data.get('email');
		const password = data.get('password');

		let requestOk = false;
		let customer;
		await fetch('https://localhost:7067/api/customer/login', {
			method: 'POST',
			body: JSON.stringify({
				email: email,
				password: password
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
			.then(async (response) => {
				if (response.status === 200) {
					requestOk = true;
				}
				await response.json().then((user) => {
					customer = user;
				});
			})
			.catch((error) => {
				console.log('error', error);
				return [];
			});

		return { success: requestOk, customer: customer };
	}
};
