import type { Reservation } from '$lib/types';
import type { ReservationDB, ReservationInput } from './types';

export const makeReservation = async (
	reservation: ReservationInput
): Promise<{ success: boolean }> => {
	let requestOk = false;
	await fetch('https://localhost:7067/api/reservation', {
		method: 'POST',
		body: JSON.stringify(reservation),
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			if (response.status === 201) {
				requestOk = true;
			}
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});

	return { success: requestOk };
};

export const getReservationById = async (
	reservationID: string
): Promise<{ success: boolean; reservations?: Reservation[] }> => {
	let requestOk = false;
	let reservations = undefined;
	await fetch('https://localhost:7067/api/reservation/' + reservationID, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			if (response.status === 200) {
				requestOk = true;
			}
			await response.json().then((user) => {
				reservations = user;
			});
		})
		.catch((error) => {
			console.log('error', error);
		});

	return { success: requestOk, reservations: reservations };
};

export const getReservationsByCustomerId = async (
	customerID: string
): Promise<{ success: boolean; reservations?: ReservationDB[] }> => {
	let requestOk = false;
	let reservations = undefined;
	await fetch('https://localhost:7067/api/reservation/customer/' + customerID, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			if (response.status === 200) {
				requestOk = true;
			}
			await response.json().then((user) => {
				reservations = user;
			});
		})
		.catch((error) => {
			console.log('error', error);
		});

	return { success: requestOk, reservations: reservations };
};

export const updateReservationStatus = async (
	reservationID: string,
	status: number
): Promise<{ success: boolean }> => {
	let requestOk = false;
	await fetch(
		'https://localhost:7067/api/reservation/' + reservationID + '/status?status=' + status,
		{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			}
		}
	)
		.then(async (response) => {
			if (response.status === 200) {
				requestOk = true;
			}
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});

	return { success: requestOk };
};
