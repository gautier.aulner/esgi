import type { CustomerID } from '../customer/types';

export type ReservationDB = {
	customerID: CustomerID;
	reservationID: ReservationID;
	checkInDate: string;
	numberOfNights: number;
	status: number;
	roomType: number;
};

export type ReservationInput = {
	customerID: CustomerID;
	checkInDate: string;
	numberOfNights: number;
	roomType: number;
};

export type ReservationID = {
	value: string;
};

export type ReservationStatus = 'RESERVED' | 'CONFIRMED' | 'CANCELLED';
export const ReservationStatuses: ReservationStatus[] = ['RESERVED', 'CONFIRMED', 'CANCELLED'];

export const getStatusIndex = (status: ReservationStatus): number =>
	ReservationStatuses.indexOf(status);
