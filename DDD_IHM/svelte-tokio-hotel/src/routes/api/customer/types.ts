export type Customer = {
	customerID: CustomerID;
	fullName: string;
	password: string;
	email: string;
	phoneNumber: string;
	wallet: Wallet;
};

export type Wallet = {
	balance: number;
	currency: ISO2Currency;
};

export type CustomerID = {
	value: string;
};

export type ISO2Currency = 'EUR' | 'USD' | 'GBP' | 'JPY' | 'CHF';
export const ISO2Currencies: ISO2Currency[] = ['EUR', 'USD', 'GBP', 'JPY', 'CHF'];

export const getCurrencyIndex = (currency: ISO2Currency): number =>
	ISO2Currencies.indexOf(currency);
