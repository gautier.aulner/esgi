import { getCurrencyIndex, type Customer, type Wallet } from './types';

export const getCustomerById = async (
	id: string
): Promise<{ success: boolean; customer?: Customer }> => {
	let requestOk = false;
	let customer = undefined;
	await fetch('https://localhost:7067/api/customer/' + id, {
		method: 'GET',
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			if (response.status === 200) {
				requestOk = true;
			}
			await response.json().then((user) => {
				customer = user;
			});
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});

	return { success: requestOk, customer: customer };
};

export const updateWallet = async (id: string, wallet: Wallet): Promise<{ success: boolean }> => {
	let requestOk = false;
	let customer;
	await fetch('https://localhost:7067/api/customer/' + id + '/wallet', {
		method: 'PUT',
		body: JSON.stringify({
			balance: wallet.balance,
			currency: getCurrencyIndex(wallet.currency)
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
		.then(async (response) => {
			if (response.status === 200) {
				requestOk = true;
			}
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});

	return { success: requestOk };
};

export const checkIfAmountValid = async (
	customerID: string,
	totalPrice: number
): Promise<{ success: boolean }> => {
	let requestOk = false;
	await fetch(
		'https://localhost:7067/api/customer/' +
			customerID +
			'/wallet/check?amountToCheck=' +
			totalPrice,
		{
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}
		}
	)
		.then(async (response) => {
			if (response.ok) {
				requestOk = true;
			}
		})
		.catch((error) => {
			console.log('error', error);
			return [];
		});
	return { success: requestOk };
};
