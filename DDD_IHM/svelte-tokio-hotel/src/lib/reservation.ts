import type { ReservationInput } from '../routes/api/reservation/types';
import type { RoomType } from './types';

export const addItemToCart = (reservation: ReservationInput) => {
	const cart = getCart();
	cart.push(reservation);
	sessionStorage.setItem('cart', JSON.stringify(cart));
};

export const getCart = (): ReservationInput[] => {
	const cart = sessionStorage.getItem('cart');
	return cart ? JSON.parse(cart) : [];
};

export const removeItemFromCart = (id: number) => {
	const cart = getCart();
	const newCart = cart.filter((item, index) => {
		return index !== id;
	});
	sessionStorage.setItem('cart', JSON.stringify(newCart));
};

export const getCartSize = (): number => {
	const cart = getCart();
	return cart.length;
};

export const clearCart = () => {
	if (!sessionStorage.getItem('cart')) return;
	sessionStorage.removeItem('cart');
};
