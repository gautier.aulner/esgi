export function upperToUpperFirst(word: string) {
	if (word.length === 0) {
		return word;
	}

	return word.charAt(0).toUpperCase() + word.slice(1).toLowerCase();
}

// export function getRoomType(roomType: RoomType) {
// 	switch (roomType) {
// 		case 'STANDARD':
// 			return 'Standard';
// 		case 'SUPERIOR':
// 			return 'Supérieur';
// 		case 'SUITE':
// 			return 'Suite';
// 	}
// }

export function getRoomTypeInt(roomType: number): string {
	switch (roomType) {
		case 0:
			return 'Standard';
		case 1:
			return 'Supérieur';
		case 2:
			return 'Suite';
		default:
			return 'Standard';
	}
}

export function getRoomStatusInt(status: number): string {
	switch (status) {
		case 0:
			return 'Réservée';
		case 1:
			return 'Confirmée';
		case 2:
			return 'Annulée';
		default:
			return 'Réservée';
	}
}

export function divisionEntiere(arrondir: boolean, dividende: number, diviseur: number) {
	let resultat = dividende / diviseur;
	return arrondir ? Math.round(resultat) : resultat;
}
