import type { ReservationInput } from '../routes/api/reservation/types';

export const getRoomImage = (roomId: number) => {
	switch (roomId) {
		case 0:
			return 'standard.jpg';
		case 1:
			return 'superior.jpg';
		case 2:
			return 'suite.jpg';
	}
};

export const getRoomName = (roomId: number) => {
	switch (roomId) {
		case 0:
			return 'Standard';
		case 1:
			return 'Superieure';
		case 2:
			return 'Suite';
	}
};

export const getRoomPrice = (roomId: number): number => {
	switch (roomId) {
		case 0:
			return 50;
		case 1:
			return 100;
		case 2:
			return 200;
		default:
			return 50;
	}
};

export const getTotalPrices = (rooms: ReservationInput[]): number => {
	return rooms.reduce((acc, room) => {
		return acc + getRoomPrice(room.roomType);
	}, 0);
};
