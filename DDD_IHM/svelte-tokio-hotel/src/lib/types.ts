import type { ReservationID } from '../routes/api/reservation/types';

export type Reservation = {
	id: ReservationID;
	checkInDate: string;
	numberOfNights: number;
	status: number;
	roomType: number;
	equipments: string[];
	price: number;
};

// export type RoomType = 'STANDARD' | 'SUPERIOR' | 'SUITE';

export type RoomStatus = 'RESERVED' | 'CONFIRMED' | 'CANCELLED';
